package main

import "fmt"

func main(){
	message1 := "Hello"
	message2 := "World"

	// Println
	fmt.Println(message1, message2)
	fmt.Println(message1, message2)

	// Printf
	message1 = "Google"
	employees := 5000 
	fmt.Printf("%s has %d employees\n",message1, employees) // %v if you don't know the data type. 

	// Sprintf
	message := fmt.Sprintf("%s has %d employees",message1, employees)
	fmt.Println(message)

	// Data Type
	fmt.Printf("%T %T\n", message1, employees)
}