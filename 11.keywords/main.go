package main

import "fmt"

func main(){

	// defer
	defer fmt.Println("Hello") // The last condition that the function executes.
	fmt.Println("World")

	// Continue - break
	for i := 0; i < 10; i++{
		if i == 0{
			fmt.Println("It's zero")
			continue
		}

		if i == 8 {
			fmt.Println("break")
			break
		}

		fmt.Println(i)
	}
}