package main

import "fmt"

type car struct {
	brand string
	model int
}

func main(){

	car1 := car{model:2012, brand:"BMW"}
	fmt.Println(car1)

	// other way
	var car2 car
	car2.model = 2020
	car2.brand = "Mazda"
	fmt.Println(car2)
}