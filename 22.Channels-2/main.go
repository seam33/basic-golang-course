package main 

import "fmt"

func message(text string, c chan<- string){
	c<-text
}

func main(){
	c := make(chan string, 2)
	c<- "Message1"
	c<- "Message2"

	// len - cap
	fmt.Println(len(c), cap(c))

	// Range y close
	close(c)

	//c<- "Message3"

	for message := range c{
		fmt.Println(message)
	}

	//select
	email1 := make(chan string)
	email2 := make(chan string)
	go message("message1", email1)
	go message("message2", email2)

	for i := 0; i < 2; i++{
		select {
		case m1 := <- email1:
			fmt.Println("From email1",m1)
		case m2 := <- email2:
			fmt.Println("From email2",m2)
		}
	}
	
}