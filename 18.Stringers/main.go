package main 

import "fmt"

type person struct{
	age int
	name string
	nationality string
}

func (human person) String() string{
	return fmt.Sprintf("%s is %d years old", human.name, human.age)
}

func main(){
	person1 := person{age: 32, nationality: "Colombian", name: "Andres"}
	fmt.Println(person1)
}