package main

import "fmt"

func main(){
	m := make(map[string]int)

	m["Colombia"]=57
	m["Mexico"]=52

	for key,value := range m{
		fmt.Println(key,value)
	}

	fmt.Println(m["Colombia"])
	
	// Interesting
	value, ok := m["Venezuela"]

	fmt.Println(value,ok)
}