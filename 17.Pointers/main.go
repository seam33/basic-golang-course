package main 

import "fmt"
type person struct{
	age int
	name string
	nationality string
}

func (human person) ping(){
	fmt.Println(human.name, human.age, human.nationality)
}

func (human *person) changeAge(){
	human.age+=1
	fmt.Println(human.name, human.age, human.nationality)
}

func main(){

	a := 50
	b := &a
	fmt.Println(a,b,*b)

	*b = 100
	fmt.Println(a,b,*b)

	person1 := person{age: 32, nationality: "Colombian", name: "Sebastian"}
	fmt.Println(person1)

	person1.ping()
	person1.changeAge()
}