package main
import "fmt"

func main(){
	// Integers
	var a int
	var b int8
	var c int16
	var d int32
	var e int64

	// Unsigned Integers
	var f uint
	var g uint8
	var h uint16
	var i uint32
	var j uint64

	// Float
	var k float32
	var l float64

	// String
	var m string // " "
	var n bool 

	// Complex - 10 + 8i
	var o complex64 // Real and imaginary float 32
	var p complex128 // Real and imaginary float 64

	fmt.Println(a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p)
}