package main

import "fmt"

func main(){
	print(1)
	print(returnValue(1,2))

	val1, val2 := dobleReturn(2)
	fmt.Println(val1,val2)

	_, val3 := dobleReturn(4)
	fmt.Println(val3)
}

func print(number int){
	fmt.Printf("Hello World %d\n", number)
}

func returnValue(a,b int) int{
	return a*b*2
}

func dobleReturn(a int) (c,d int){
	return a,a*2
}