package main

import "fmt"

func main(){

	// Conditional for
	for i := 0; i < 3; i++{
		fmt.Println(i)
	}

	fmt.Println()

	// for while 
	var counter int
	for counter <= 5{
		fmt.Println(counter)
		counter++
	}

	fmt.Println()

	// for forever
	var infinite int 
	
	for {
		fmt.Println(infinite)
		infinite++
	}

}