package mypackage

// Private
type car struct {
	brand string
	model int
}

// CarPublic - public (Uppercase attributes)
type CarPublic struct {
	Brand string
	Model int
}