package main

import(
	"fmt"
	"strings"
)

func main(){
	slice := []string{"one","two","three"}

	for _, valor := range slice{
		fmt.Println(valor)
	}

	condition := isPalindrome("Civic")
	fmt.Println(condition)

}

func isPalindrome(text string) bool{
	text = strings.ToLower(text)
	var reverse string

	for i := len(text)-1; i>=0; i--{
		reverse += string(text[i])
	}

	if reverse == text{
		return true
	}else{
		return false
	}
}