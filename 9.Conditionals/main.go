package main

import "fmt"

func main()  {
	valor1 := 3
	valor2 := 2

	if valor1 == 1{
		fmt.Println("is 1.")
	}else if valor1 == valor2 {
		fmt.Println("equals valor2.")
	}else{
		fmt.Println("It's different.")
	}

	if valor1 == 3 && valor2 == 2{
		fmt.Println("test &&")
	}else if valor1 > 0 || valor2 != 2{
		fmt.Println("test ||")
	}else{
		fmt.Println("Nothing more to see.")
	}
}