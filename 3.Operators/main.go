package main

import "fmt"
import "math"

func main(){

	x := 10
	y := 50

	// Sum
	result := x+y
	fmt.Println("Sum:",result)
	
	// Subtraction
	result = x-y
	fmt.Println("Subtraction:",result)

	// Multiplication
	result = x*y
	fmt.Println("Multiplication:",result)

	// Division 
	result = y/x
	fmt.Println("Division:",result)
	
	//Module
	result = y%x
	fmt.Println("Module:",result)

	//Increase
	x++
	fmt.Println("Increase:",x)

	//Decrement
	x--
	fmt.Println("Decrement:",x)

	//Area - Rectangle - trapeze - circle 
	
	// Rectangle
	const side int = 13
	fmt.Println("Area is:", side*side)

	// Trapeze
	b1 := 3.4
	b2 := 2.3
	h := 2.1
	fmt.Println("Area is:", 0.5*h*(b1+b2))

	// Circle
	const PI float64 = 3.1415
	const radio = 3
	fmt.Println("Area is:", PI*math.Pow(radio,2))
}