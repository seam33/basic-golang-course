package main

import "fmt"

//chan<- Input
//<-chan Output
func say(text string,c chan<- string){
	c <- text
}

func main(){
	c := make(chan string, 1)

	fmt.Println("Hello")

	go say("Good bye!", c)
	fmt.Println(<-c) // Channel Output
}