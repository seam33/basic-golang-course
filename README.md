# Summary

<img src="/icon/golang_icon.png"/>

- Learning exercises to improve skills in the Golang Programming Language.

## Content

1. Hello-World
2. Variables
3. Operators
4. Data Types
5. Package fmt
6. Functions
7. Documentation
8. Cycles -- Only For
9. Conditionals 
10. Switch
11. Keywords
12. Arrays/Slices
13. Slices with Rage
14. Maps
15. Strucs
16. Encapsulation
17. Pointers
18. Stringers
19. Interface
20. Concurrency
21. Concurrency - Channels 1
22. Concurrency - Channels 2
23. Package Manager = go get

## Source

- <a href="https://tour.golang.org/welcome/1">Go tour</a>
- <a href="https://golangweekly.com">Golang Weekly</a>
- <a href="https://gobyexample.com">Go by example</a>