package main

import "fmt"

func main(){
	// Array
	var array [4]int
	fmt.Println(array)
	array[0] = 1
	fmt.Println(array)

	// len + cap
	fmt.Println("len:",len(array),"cap:", cap(array))

	// Slice 
	slice := []int{0,1,2,3,4,5}
	fmt.Println(slice,"len:",len(slice),"cap:", cap(slice))

	// Slice methods
	fmt.Println(slice[0], slice[:3], slice[2:4], slice[4:])

	slice = append(slice,7,7,7,7)
	fmt.Println(slice)

	newSlice := []int{8,3,8,3}
	slice = append(slice, newSlice...)
	fmt.Println(slice)
}