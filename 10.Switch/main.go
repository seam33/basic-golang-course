package main

import "fmt"

func main()  {
	valor1 := 3

	switch(valor1){
	case 1:
		fmt.Println("is 1")
	case 2:
		fmt.Println("is 2")
	case 3:
		fmt.Println("is 3")
	default:
		fmt.Println("......")
	}

	valor := 100

	switch{
	case valor > 100:
		fmt.Println(">100")
	case valor < 100:
		fmt.Println("<100")
	case valor == 100:
		fmt.Println("=100")
	default:
		fmt.Println("......")
	}


}