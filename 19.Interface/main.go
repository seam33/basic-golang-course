package main 

import(
	"math"
	"fmt"
)

type figures2D interface{
	area() float64
}

type square struct{
	base float64
}

type rectangle struct{
	base float64
	height float64
}

func (a square) area() float64{
	return math.Pow(a.base,2)
}

func (r rectangle) area() float64{
	return r.base * r.height
}

func calculate(f figures2D) {
	fmt.Println("Area is", f.area())
}

func main(){
	square1 := square{base:5}
	rectangle1 := rectangle{base:5, height:12}

	calculate(square1)
	calculate(rectangle1)

	//List of Interfaces
	myInterface := []interface{}{"Hola", true, 12}

	fmt.Println(myInterface...)

}