package main

import "fmt"

func main(){
	//Constants
	const PI = 3.14
	const PI2 float64 = 3.1415
	fmt.Println(PI,PI2)

	// other variables
	weight := 12 
	var height int = 14
	var area int 
	area = weight * height

	fmt.Println(weight,height,area)

	// Zero Values
	var a int 
	var b float64
	var c string
	var d bool 

	fmt.Println(a,b,c,d)

	// Square area
	const base = 10
	area2 := base * base
	fmt.Println(area2)
}